inputJ = open('J/inputJ.txt', 'r+')
lines = inputJ.readlines()
inputJ.close()

rules = dict()
for i in range(0, 136):
    temp = lines[i]
    rule = temp.split(':')
    rule[1] = rule[1].strip().replace('\n', '')
    rule[1] = rule[1].split(' ')
    rules.update({int(rule[0]): rule[1]})

inputs = []
for i in range(137, len(lines)):
    inputs.append(lines)
"""
inputJ = open('J/test.txt', 'r+')
lines = inputJ.readlines()
inputJ.close()

rules = dict()
for i in range(0, 6):
    temp = lines[i]
    rule = temp.split(':')
    rule[1] = rule[1].strip().replace('\n', '')
    rule[1] = rule[1].split(' ')
    rules.update({int(rule[0]): rule[1]})

inputs = []
for i in range(7, len(lines)):
    inputs.append(lines)
""""""
temp = rules.get(100) + rules.get(116)
print(temp)
idx = temp.index('|')
new_list1 = temp[:idx] + temp[idx+3:]
print(new_list1)
new_list2 = temp[idx+1:]
print(new_list2)
"""
start = [rules.get(0)]
finished = []

while len(start) > 0:
    temp = start.pop(0)
    if '|' in temp:
        idx = temp.index('|')
        new_list1 = temp[:idx] + temp[idx+3:]
        new_list2 = temp[:idx-2] + temp[idx+1:]
        start.append(new_list1)
        start.append(new_list2)
        continue
    test = set(temp)
    if 'a' in test and len(test) == 1:
        finished.append(temp)
        continue
    if 'b' in test and len(test) == 1:
        finished.append(temp)
        continue
    if 'a' in test and 'b' in test and len(test) == 2:
        finished.append(temp)
        continue
    """
    i = 0
    while i < len(temp) and (temp[i] == 'a' or temp[i] == 'b'):
        i += 1
    i += 1
    """"""
    for l in range(0, len(temp)):
        if temp[l] == 'a' or temp[l] == 'b':
            i += 1
    if i == len(temp):
        finished.append(temp)
        continue
    """
    for i in range(0, len(temp)):
        if temp[i] == 'a':
            continue
        if temp[i] == 'b':
            continue
        rule_num = int(temp[i])
        rule = rules.get(rule_num)
        new_list = temp[:i] + rule + temp[i+1:]
        start.append(new_list)
        break

print(finished[0])

    