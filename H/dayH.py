from copy import deepcopy

inputH = open('H/inputH.txt', 'r+')
lines = inputH.readlines()
inputH.close()

for i in range(0, len(lines)):
    lines[i] = lines[i].replace('\n', '')

class EnergySource:
    def __init__(self, lines):
        self.cube = [lines]

    def expandCube(self):
        for i in range(0, len(self.cube)):
            for j in range(0, len(self.cube[i])):
                self.cube[i][j] = '.' + self.cube[i][j] + '.'
            self.cube[i].insert(0, '.' * len(self.cube[i][0]))
            self.cube[i].insert(len(self.cube[i]), '.' * len(self.cube[i][0]))
        new_layer = ['.' * len(self.cube[0][0]) for i in range(0, len(self.cube[0]))]
        self.cube.insert(0, deepcopy(new_layer))
        self.cube.append(deepcopy(new_layer))

    def changeState(self):
        new_state = deepcopy(self.cube)
        for z in range(1, len(self.cube)-1):
            for y in range(1, len(self.cube[z])-1):
                for x in range(1, len(self.cube[z][y])-1):
                    activeNeighbors = self.countActiveNeighbors(z, y, x)
                    if self.cube[z][y][x] == '#':
                        if not (activeNeighbors == 2 or activeNeighbors == 3):
                            new_state[z][y] = new_state[z][y][:x] + '.' + new_state[z][y][x+1:]
                    else:
                        if activeNeighbors == 3:
                            new_state[z][y] = new_state[z][y][:x] + '#' + new_state[z][y][x+1:]
        self.cube = deepcopy(new_state)

    def countActiveNeighbors(self, z, y, x):
        counter = 0
        for i in range(z-1, z+2):
            for j in range(y-1, y+2):
                for k in range(x-1, x+2):
                    if self.cube[i][j][k] == '#':
                        counter += 1
        if self.cube[z][y][x] == '#':
            counter -= 1
        return counter

    def countAllActive(self):
        counter = 0
        for layer in self.cube:
            for line in layer:
                for char in line:
                    if char == '#':
                        counter += 1
        return counter

    def printAll(self):
        for i in range(1, len(self.cube)-1):
            print(i)
            for j in range(1, len(self.cube[i])-1):
                print(self.cube[i][j][1:len(self.cube[i][j])-1])

es = EnergySource(lines)

#Add neutral padding for all dimensions 
es.expandCube()

for i in range(0, 6):
    es.expandCube()
    es.changeState()

#es.printAll()
print(es.countAllActive())