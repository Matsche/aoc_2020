inputE = open('E/inputE.txt', 'r+')
lines = inputE.readlines()
inputE.close()

mem = dict()
mask = ''

for line in lines:
    if line[:4] == 'mask':
        temp = line.split('=')
        mask = temp[1].strip().replace('\n', '')
    else:
        temp = line.split('=')
        numb = temp[1].strip()
        bina = '{0:b}'.format(int(numb))
        bina = '0' * (36-len(bina)) + bina
        addr = temp[0].strip()
        addr = addr[4:]
        addr = addr.replace(']', '')
        masked = ''
        for i in range(0, len(mask)):
            if mask[i] != 'X':
                masked += mask[i]
            else:
                masked += bina[i]
        mem.update({addr: masked})

sum = 0
for val in mem.values():
    sum += int(val, 2)
print(sum)


def updateAllAddr(numb, addr, mem):
    addrs = [addr]
    finished_addrs = []
    while len(addrs) != 0:
        no_update = True
        temp = addrs.pop()
        for i in range(0, len(temp)):
            if temp[i] == 'X':
                new_addr1 = temp[:i] + '0' + temp[i+1:]
                new_addr2 = temp[:i] + '1' + temp[i+1:]
                addrs.append(new_addr1)
                addrs.append(new_addr2)
                no_update = False
                break
        if no_update == True:
            finished_addrs.append(temp)
    for i in finished_addrs:
        mem.update({i: numb})
    return mem

mem = dict()
mask = ''

for line in lines:
    if line[:4] == 'mask':
        temp = line.split('=')
        mask = temp[1].strip().replace('\n', '')
    else:
        temp = line.split('=')
        numb = int(temp[1].strip())
        addr = temp[0].strip()
        addr = addr[4:]
        addr = addr.replace(']', '')
        addr = '{0:b}'.format(int(addr))
        addr = '0' * (36-len(addr)) + addr
        masked = ''
        for i in range(0, len(mask)):
            if mask[i] == '0':
                masked += addr[i]
            elif mask[i] == '1':
                masked += '1'
            else:
                masked += 'X'
        mem = updateAllAddr(numb, masked, mem)

sum = 0
for val in mem.values():
    sum += val
print(sum)
