#Tree does not work cause python dies
class Node:
    def __init__(self, value):
        self.value = value
        self.children = []

def createTree(node, numbers):
    i = numbers.index(node.value)
    for j in range(i+1, len(numbers)):
        if numbers[j] - numbers[i] < 4:
            node.children.append(Node(numbers[j]))
        else:
            break
    for child in node.children:
        #print(child.value)
        createTree(child, numbers)

def countBranches(node):
    sum = 0
    if len(node.children) == 0:
        return 1
    else:
        for child in node.children:
            sum += countBranches(child)
        return sum

inputA = open('A/inputA.txt', 'r+')
lines = inputA.readlines()
inputA.close()

numbers = [int(line) for line in lines]
numbers.sort()

numbers.insert(0, 0)
numbers.append(numbers[-1]+3)

diff_one = [numbers[i+1] - numbers[i] == 1 for i in range(0, len(numbers)-1)]
diff_three = [numbers[i+1] - numbers[i] == 3 for i in range(0, len(numbers)-1)]
print(diff_one.count(True) * diff_three.count(True))
"""
root = Node(numbers[0])
createTree(root, numbers)
print(countBranches(root))
"""

solutions = [0 for i in numbers]
solutions[0] = 1
for i in range (0, len(numbers)):
    for j in range(i+1, len(numbers)):
        if numbers[j] - numbers[i] > 3:
            break
        solutions[j] += solutions[i]
print(solutions[-1])
