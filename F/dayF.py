numbers = [6,13,1,15,2,0]
elements = 30000000

last_exits = dict()
for i in range(0, len(numbers)):
    last_exits.update({numbers[i]: i+1})

last_number = numbers[-1]

for i in range(len(numbers), elements):
    if last_number not in last_exits.keys():
        last_exits.update({last_number: i})
        last_number = 0
    else:
        last_time = last_exits.get(last_number)
        last_exits.update({last_number: i})
        last_number = i - last_time

print(last_number)