inputM = open('M/22_input.txt', 'r+')
lines = inputM.readlines()
inputM.close()

deck1 = []
deck2 = []

for i in range(1, 26):
    deck1.append(int(lines[i]))

for i in range(28, len(lines)):
    deck2.append(int(lines[i]))

recursive_deck1 = deck1.copy()
recursive_deck2 = deck2.copy()

while 1:
    card1 = deck1.pop(0)
    card2 = deck2.pop(0)
    if card1 > card2:
        deck1.append(card1)
        deck1.append(card2)
    else:
        deck2.append(card2)
        deck2.append(card1)

    if len(deck1) == 0 or len(deck2) == 0:
        break

counter = 0
if len(deck1) != 0:
    idx = len(deck1)
    for card in deck1:
        counter += (card * idx)
        idx -= 1
else:
    idx = len(deck2)
    for card in deck2:
        counter += (card *idx)
        idx -= 1

print(counter)

def playRecursive(deck1, deck2):
    prev_decks1 = []
    prev_decks2 = []

    while 1:
        if deck1 in prev_decks1 or deck2 in prev_decks2:
            return 1
        else:
            prev_decks1.append(deck1.copy())
            prev_decks2.append(deck2.copy())

        card1 = deck1.pop(0)
        card2 = deck2.pop(0)

        if len(deck1) >= card1 and len(deck2) >= card2:
            new_deck1 = []
            new_deck2 = []
            for i in range(0, card1):
                new_deck1.append(deck1[i])
            for i in range(0, card2):
                new_deck2.append(deck2[i])
            winner = playRecursive(new_deck1, new_deck2)
            if winner == 1:
                deck1.append(card1)
                deck1.append(card2)
            else:
                deck2.append(card2)
                deck2.append(card1)
        else:
            if card1 > card2:
                deck1.append(card1)
                deck1.append(card2)
            else:
                deck2.append(card2)
                deck2.append(card1)

        if len(deck1) == 0 or len(deck2) == 0:
            break
    
    if len(deck1) != 0:
        return 1
    else:
        return 2

counter2 = 0
winner = playRecursive(recursive_deck1, recursive_deck2)
if winner == 1:
    idx = len(recursive_deck1)
    for card in recursive_deck1:
        counter2 += (card * idx)
        idx -= 1
else:
    idx = len(recursive_deck2)
    for card in recursive_deck2:
        counter2 += (card *idx)
        idx -= 1
print(counter2)