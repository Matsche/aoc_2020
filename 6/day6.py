input6 = open('6/input6.txt', 'r+')
lines = input6.readlines()
input6.close()

entries = []
temp_string = ''
for line in lines:
    if line == '\n':
        entries.append(temp_string.strip())
        temp_string = ''
    else:
        temp_string = temp_string + line.replace('\n', ' ')
sum1 = 0
sum2 = 0
for entry in entries:
    sum1 += len(set(entry))
    whole_set = set(entry)
    anwsers = entry.split(' ')
    for aws in anwsers:
        whole_set.intersection_update(aws)
    sum2 += len(whole_set)
print(sum1)
print(sum2)