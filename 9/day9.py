import itertools

input9 = open('9/input9.txt', 'r+')
lines = input9.readlines()
input9.close()

numbers = [int(line) for line in lines]
answer1 = 0

for i in range (25, len(numbers)):
    has_sum = False
    for j in range(i-25, i):
        for k in range(j+1, i):
            if numbers[i] == numbers[j] + numbers[k]:
                has_sum = True
    if has_sum == False:
        answer1 = numbers[i]
        print(numbers[i])
        break

for i in range(0, len(numbers)):
    sum = numbers[i]
    j = i + 1
    con_numbers = [numbers[i]]
    while sum <= answer1:
        if sum == answer1:
            break
        else:
            sum += numbers[j]
            con_numbers.append(numbers[j])
            j += 1
    if sum == answer1:
        con_numbers.sort()
        print(con_numbers[0] + con_numbers[-1])
        break