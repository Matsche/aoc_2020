#include <stdio.h>
#include <stdlib.h>

int getPrevOccuIdx(int* numbers, int last_number, int last_idx) {
    for(int i = last_idx-1; i >= 0; i--) {
        if(numbers[i] == last_number) {
            return i;
        }
    }
    return -1;
}

int hasMoreThanOne(int* numbers, int last_number, int last_idx) {
    int counter = 0;
    for(int i = 0; i <= last_idx; i++) {
        if(numbers[i] == last_number) {
            counter++;
        }
        if(counter > 1) {
            return 1;
        }
    }
    return 0;
}

int main() {
    int elements = 30000000;
    int* numbers = (int*) malloc(elements * sizeof(int));
    for(int i = 0; i < elements; i++)
        numbers[i] = 0;
    //6,13,1,15,2,0
    numbers[0] = 6;
    numbers[1] = 13;
    numbers[2] = 1;
    numbers[3] = 15;
    numbers[4] = 2;
    numbers[5] = 0;
    int last_number = 0;
    for(int i = 6; i < elements; i++) {
        int moreThanOne = hasMoreThanOne(numbers, last_number, i-1);
        if(moreThanOne == 1) {
            int prev_idx = getPrevOccuIdx(numbers, last_number, i-1);
            int next_number = (i-1) - prev_idx;
            numbers[i] = next_number;
            last_number = next_number;
        }
        else {
            numbers[i] = 0;
            last_number = 0;
        }
    }
    printf("%d\n", numbers[elements-1]);
    return numbers[elements-1];
}