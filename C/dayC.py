import math

def rotateLeft(facing, angle):
    for i in range(0, angle, 90):
        if facing == 'E':
            facing = 'N'
        elif facing == 'N':
            facing = 'W'
        elif facing == 'W':
            facing = 'S'
        elif facing == 'S':
            facing = 'E'
    return facing

def rotateRight(facing, angle):
    for i in range(0, angle, 90):
        if facing == 'E':
            facing = 'S'
        elif facing == 'S':
            facing = 'W'
        elif facing == 'W':
            facing = 'N'
        elif facing == 'N':
            facing = 'E'
    return facing

def rotateWaypointRight(waypoint_north, waypoint_east, angle):
    x = waypoint_east
    y = waypoint_north
    new_x = x * int(math.cos(math.radians(angle))) + y * int(math.sin(math.radians(angle)))
    new_y = -x * int(math.sin(math.radians(angle))) + y * int(math.cos(math.radians(angle)))
    return new_y, new_x

def rotateWaypointLeft(waypoint_north, waypoint_east, angle):
    angle = 360 - angle
    x = waypoint_east
    y = waypoint_north
    new_x = x * int(math.cos(math.radians(angle))) + y * int(math.sin(math.radians(angle)))
    new_y = -x * int(math.sin(math.radians(angle))) + y * int(math.cos(math.radians(angle)))
    return new_y, new_x

inputC = open('C/inputC.txt', 'r+')
lines = inputC.readlines()
inputC.close()

directions = []

for line in lines:
    directions.append(tuple((line[:1], int(line[1:]))))

north = 0
east = 0
facing = 'E'

for direction in directions:
    if direction[0] == 'N':
        north += direction[1]
    elif direction[0] == 'S':
        north -= direction[1]
    elif direction[0] == 'E':
        east += direction[1]
    elif direction[0] == 'W':
        east -= direction[1]
    elif direction[0] == 'F':
        if facing == 'N':
            north += direction[1]
        elif facing == 'S':
            north -= direction[1]
        elif facing == 'E':
            east += direction[1]
        elif facing == 'W':
            east -= direction[1]
    elif direction[0] == 'L':
        facing = rotateLeft(facing, direction[1])
    elif direction[0] == 'R':
        facing = rotateRight(facing, direction[1])

print(abs(north) + abs(east))


waypoint_north = 1
waypoint_east = 10
north = 0
east = 0

for direction in directions:
    if direction[0] == 'N':
        waypoint_north += direction[1]
    elif direction[0] == 'S':
        waypoint_north -= direction[1]
    elif direction[0] == 'E':
        waypoint_east += direction[1]
    elif direction[0] == 'W':
        waypoint_east -= direction[1]
    elif direction[0] == 'F':
        for i in range(0, direction[1]):
            north += waypoint_north
            east += waypoint_east
    elif direction[0] == 'L':
        waypoint_north, waypoint_east = rotateWaypointLeft(waypoint_north, waypoint_east, direction[1])
    elif direction[0] == 'R':
        waypoint_north, waypoint_east = rotateWaypointRight(waypoint_north, waypoint_east, direction[1])

print(abs(north) + abs(east))