def isAdjacentEmpty(line_index, char_index, lines):
    y = line_index
    x = char_index
    if lines[y-1][x-1] != '#' and lines[y-1][x] != '#' \
        and lines[y-1][x+1] != '#' and lines[y][x-1] != '#' \
        and lines[y][x+1] != '#' and lines[y+1][x-1] != '#' \
        and lines[y+1][x] != '#' and lines[y+1][x+1] != '#':
        return True
    return False

def countAdjacentOccupied(line_index, char_index, lines):
    y = line_index
    x = char_index
    occupied = 0
    if lines[y-1][x-1] == '#':
        occupied += 1
    if lines[y-1][x] == '#':
        occupied += 1
    if lines[y-1][x+1] == '#':
        occupied += 1
    if lines[y][x-1] == '#':
        occupied += 1
    if lines[y][x+1] == '#':
        occupied += 1
    if lines[y+1][x-1] == '#':
        occupied += 1
    if lines[y+1][x] == '#':
        occupied += 1
    if lines[y+1][x+1] == '#':
        occupied += 1
    return occupied

class VisibleSeats:
    def __init__(self, lines):
        self.visisbleSeatIndex = [[[] for j in range(0, len(lines[0]))] for i in range(0, len(lines))]
        for i in range(1, len(lines)-1):
            for j in range(1, len(lines[i])-1):
              temp = self.createVisibleSeats(i, j, lines)
              self.visisbleSeatIndex[i][j].append(temp) 

    def getVisibleSeats(self, line_index, char_index):
        return self.visisbleSeatIndex[line_index][char_index]

    def createVisibleSeats(self, line_index, char_index, lines):
        y = line_index
        x = char_index
        visible_seats = []
        #rechts:
        for i in range(x+1, len(lines[y])):
            if lines[y][i] != '.':
                visible_seats.append(tuple((y,i)))
                break
        #links:
        for i in range(x-1, 0, -1):
            if lines[y][i] != '.':
                visible_seats.append(tuple((y,i)))
                break
        #oben:
        for i in range(y-1, 0, -1):
            if lines[i][x] != '.':
                visible_seats.append(tuple((i,x)))
                break
        #unten:
        for i in range(y+1, len(lines)):
            if lines[i][x] != '.':
                visible_seats.append(tuple((i,x)))
                break
        #oben rechts:
        i = y-1
        j = x+1
        while i > 0 and j < len(lines[i]):
            if lines[i][j] != '.':
                visible_seats.append(tuple((i,j)))
                break
            i -= 1
            j += 1
        #oben links:
        i = y-1
        j = x-1
        while i > 0 and j > 0:
            if lines[i][j] != '.':
                visible_seats.append(tuple((i,j)))
                break
            i -= 1
            j -= 1
        #unten rechts:
        i = y+1
        j = x+1
        while i < len(lines) and j < len(lines[i]):
            if lines[i][j] != '.':
                visible_seats.append(tuple((i,j)))
                break
            i += 1
            j += 1
        #unten links:
        i = y+1
        j = x-1
        while i < len(lines) and j > 0:
            if lines[i][j] != '.':
                visible_seats.append(tuple((i,j)))
                break
            i += 1
            j -= 1
        return visible_seats


def isSeatsEmpty(line_index, char_index, lines):
    y = line_index
    x = char_index
    visible_seats = visibleSeats.getVisibleSeats(y, x)
    for seat in visible_seats[0]:
        if lines[seat[0]][seat[1]] == '#':
            return False
    return True

def countSeatsOccupied(line_index, char_index, lines):
    y = line_index
    x = char_index
    visible_seats = visibleSeats.getVisibleSeats(y, x)
    occupied = 0
    for seat in visible_seats[0]:
        if lines[seat[0]][seat[1]] == '#':
            occupied +=1
    return occupied

inputB = open('inputB.txt', 'r+')
lines = inputB.readlines()
inputB.close()

for i in range(0, len(lines)):
    lines[i] = '.' + lines[i].replace('\n', '') + '.'
lines.insert(0, '...................................................................................................')
lines.insert(len(lines), '...................................................................................................')

old_state = lines.copy()
new_state = lines.copy()

while 1:
    old_state = new_state.copy()
    for i in range(1, len(old_state)-1):
        for j in range(1, len(old_state[i])-1):
            if old_state[i][j] == 'L':
                is_empty = isAdjacentEmpty(i, j, old_state)
                if is_empty:
                    new_state[i] = new_state[i][:j] + '#' + new_state[i][j+1:]
            elif old_state[i][j] == '#':
                occupied = countAdjacentOccupied(i, j, old_state)
                if occupied > 3:
                    new_state[i] = new_state[i][:j] + 'L' + new_state[i][j+1:]
    if old_state == new_state:
        break

counter = 0
for line in new_state:
    for char in line:
        if char == '#':
            counter += 1
print(counter)

visibleSeats = VisibleSeats(lines)

old_state = lines.copy()
new_state = lines.copy()

while 1:
    old_state = new_state.copy()
    for i in range(1, len(old_state)-1):
        for j in range(1, len(old_state[i])-1):
            if old_state[i][j] == 'L':
                is_empty = isSeatsEmpty(i, j, old_state)
                if is_empty:
                    new_state[i] = new_state[i][:j] + '#' + new_state[i][j+1:]
            elif old_state[i][j] == '#':
                occupied = countSeatsOccupied(i, j, old_state)
                if occupied > 4:
                    new_state[i] = new_state[i][:j] + 'L' + new_state[i][j+1:]
    if old_state == new_state:
        break

counter = 0
for line in new_state:
    for char in line:
        if char == '#':
            counter += 1
print(counter)
