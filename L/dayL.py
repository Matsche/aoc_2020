from copy import deepcopy

inputL = open('L/inputL.txt', 'r+')
lines = inputL.readlines()
inputL.close()

ingredient_counts = dict()
allergen_combinations = dict()
single_ingredients = set()

for line in lines:
    temp = line.split('(')
    temp[1] = temp[1].replace(')\n', '').replace('contains ', '').replace(' ', '')
    allergens = temp[1].split(',')
    ingredients = temp[0].strip().split(' ')

    for ingredient in ingredients:
        if ingredient not in ingredient_counts.keys():
            ingredient_counts.update({ingredient: 0})
        ingredient_counts[ingredient] += 1
            
    single_ingredients.update(set(ingredients))

    for allergen in allergens:
        if allergen not in allergen_combinations.keys():
            allergen_combinations.update({allergen: set(ingredients)})
        else:
            allergen_combinations[allergen].intersection_update(set(ingredients))

for k,v in allergen_combinations.items():
    print(k, ': ' + str(v))

safe_to_eat = single_ingredients.copy()
for ings in allergen_combinations.values():
    for ing in ings:
        if ing in safe_to_eat:
            safe_to_eat.remove(ing)

counter = 0
for food in safe_to_eat:
    counter += ingredient_counts.get(food)
print(counter)
