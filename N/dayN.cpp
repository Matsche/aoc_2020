#include <iostream>
#include <string>

using namespace std;

string inputN = "792845136";

struct cup {
    int val;
    cup* next;
};
typedef cup Cup;

int isInThreeCups(Cup* three_cups, int label) {
    for(Cup* c = three_cups; c != nullptr; c = c->next) {
        if(c->val == label)
            return 1;
    }
    return 0;
}

Cup* findDestinationCup(Cup* current_cup, int label) {
    cout << "Finding dest cup" << endl;
    for(Cup* c = current_cup; c != nullptr; c = c->next) {
        if(c->val == label)
        return c;
    }
    return nullptr;
}

int main() {
    Cup* last_cup = nullptr;
    Cup* current_cup = nullptr;
    for(int i = 0; i < 9; i++) {
        Cup* new_cup = new Cup;
        char temp = inputN[i];
        new_cup->val = atoi(&temp);
        new_cup->next = nullptr;
        if(i == 0) {
            current_cup = new_cup;
            last_cup = new_cup;
        }
        else{
            last_cup->next = new_cup;
            last_cup = new_cup;
        }
    }
    cout << "Finished input" << endl;
    Cup* c = current_cup;
    for(; c->next != nullptr; c = c->next);
    c->next = current_cup;
    for(int k = 0; k < 100; k++) {
        cout << "Round " << k << endl;
        Cup* t = current_cup;
        cout << "Cups: ";
        for(int j = 0; j < 9; j++) {
            cout << t->val;
            t = t->next;
        }
        cout << endl;
        Cup* three_cups = current_cup->next;
        Cup* fusion_cup = current_cup->next->next->next->next;
        current_cup->next = fusion_cup;
        three_cups->next->next->next = nullptr;
        cout << "Three cups: ";
        Cup* f = three_cups;
        for(int k = 0; k < 3; k++){
            cout << f->val;
            f = f->next;
        }
        cout << endl;
        int label = current_cup->val - 1;
        cout << "Label start: " << label << endl;
        if (label == 0) {
            label = 9;
        }
        while (isInThreeCups(three_cups, label)) {
            label--;
            if (label == 0){
                label = 9;
            }
        }
        cout << "Label after: " << label << endl;
        Cup* destination_cup = findDestinationCup(current_cup, label);
        cout << "Found dest cup" << endl;
        Cup* afterDC = destination_cup->next;
        three_cups->next->next->next = afterDC;
        destination_cup->next = three_cups;
        current_cup = current_cup->next;
    }
    Cup* e = current_cup;
    for(; e->val != 1; e = e->next);
    for(Cup* d = e->next; d->val != 1; d = d->next) {
        cout << d->val;
    }
    cout << endl;
}
