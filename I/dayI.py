inputI = open('I/inputI.txt', 'r+')
lines = inputI.readlines()
inputI.close()

def evaluteBrackets(expr, first_idx):
    count = 0
    i = first_idx
    while i < len(expr):
        first_num = -1
        if count != 0:
            first_num = count
        else:
            if expr[i] == '(':
                first_num, i = evaluteBrackets(expr, i+1)
            else:
                first_num = int(expr[i])
                i+=1
        operator = expr[i]
        i+=1
        second_num = -1
        if expr[i] == '(':
            second_num, i = evaluteBrackets(expr, i+1)
        else:
            second_num = int(expr[i])
            i += 1
        if count != 0:
            if operator == '+':
                count = count + second_num
            elif operator == '*':
                count = count * second_num
        else:
            if operator == '+':
                count = count + (first_num + second_num)
            elif operator == '*':
                count = count + (first_num * second_num)
        if expr[i] == ')':
            return count, i+1


def evaluateExpr(expr):
    count = 0
    i = 0
    while i < len(expr):
        first_num = -1
        if count != 0:
            first_num = count
        else:
            if expr[i] == '(':
                first_num, i = evaluteBrackets(expr, i+1)
            else:
                first_num = int(expr[i])
                i+=1
        operator = expr[i]
        i+=1
        second_num = -1
        if expr[i] == '(':
            second_num, i = evaluteBrackets(expr, i+1)
        else:
            second_num = int(expr[i])
            i += 1
        if count != 0:
            if operator == '+':
                count = count + second_num
            elif operator == '*':
                count = count * second_num
        else:
            if operator == '+':
                count = count + (first_num + second_num)
            elif operator == '*':
                count = count + (first_num * second_num)
        #print(count)
    return count

"""
def evaluateBracketsAdv(expr, first_idx):
    i = first_idx
    count = 0

    return count , i


def evaluateAdvanced(expr):
    i = 0
    calcMul = False
    while len(expr) > 1:
        if '+' not in expr:
            calcMul = True
            i = 0
        char = expr.pop(i)
        first_num = -1
        if char == '(':
            first_num , i = evaluateBracketsAdv(expr, i)
        else:
            first_num = int(char)
        operator = expr.pop(i)
        second_num = -1
        if operator == '*':
            if calcMul:
                for el in expr:
                    if el == '(' or ')':
                        expr.remove(el)
                char2 = expr.pop(i)
                second_num = int(char2)
                expr.insert(i, str(first_num * second_num))
            else:
                expr.insert(i, char)
                expr.insert(i+1, operator)
                i += 2
        elif operator == '+':
            char2 = expr.pop(i)
            if char2 == '(':
                second_num, i = evaluateBracketsAdv(expr, i)
            else:
                second_num = int(char2)
            expr.insert(i, str(first_num + second_num))

    return expr[0]
"""

class MyInt:
    def __init__(self, num):
        self.num = num

    def __add__(self, other):
        return self.num * other.num

    def __mul__(self, other):
        return self.num + other.num

count1 = 0
count2 = 0

for i in range(0, len(lines)):
    test = lines[i].replace('\n','').split(' ')
    fully_split = []
    while len(test) > 0:
        elem = test.pop(0)
        if len(elem) > 1:
            if elem[0] == '(':
                temp = elem.split('(')
                for j in range(0, len(temp)):
                    if temp[j] == '':
                        fully_split.append('(')
                    else:
                        fully_split.append(temp[j])
            elif elem[1] == ')':
                temp = elem.split(')')
                for j in range(0, len(temp)):
                    if temp[j] == '':
                        fully_split.append(')')
                    else:
                        fully_split.append(temp[j])
        else:
            fully_split.append(elem)
    count1 += evaluateExpr(fully_split)
    #count2 += evaluateAdvanced(fully_split)

print(count1)