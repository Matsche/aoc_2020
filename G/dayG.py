inputG = open('G/inputG.txt', 'r+')
lines = inputG.readlines()
inputG.close()

rules = []
rule_names = []
for i in range(0, 20):
    temp = lines[i].split(':')
    rule_names.append(temp[1])
    temp = temp[1].split('or')
    rule1 = temp[0].strip().split('-')
    rule2 = temp[1].strip().split('-')
    rules.append(((int(rule1[0]), int(rule1[1])) , (int(rule2[0]), int(rule2[1]))))

my_ticket = [int(num) for num in lines[22].split(',')]

other_tickets = [[int(num) for num in lines[i].split(',')] for i in range(25, len(lines))]

ticket_sc_err_r = 0
valid_tickets = other_tickets.copy()

for ticket in other_tickets:
    for field in ticket:
        valid_field = False
        for rule in rules:
            if field >= rule[0][0] and field <= rule[0][1] or field >= rule[1][0] and field <= rule[1][1]:
                valid_field = True
                break
        if valid_field == False:
            ticket_sc_err_r += field
            if ticket in valid_tickets:
                valid_tickets.remove(ticket)
print(ticket_sc_err_r)

possible_rules = [{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19} for i in range(0, len(valid_tickets[0]))]

for ticket in valid_tickets:
    for field in ticket:
        for rule in rules:
            if not (field >= rule[0][0] and field <= rule[0][1] or field >= rule[1][0] and field <= rule[1][1]):
                field_idx = ticket.index(field)
                rule_idx = rules.index(rule)
                possible_rules[rule_idx].remove(field_idx)            

for i in range(0, len(possible_rules)):
    for j in range(i+1, len(possible_rules)):
        if len(possible_rules[i]) < len(possible_rules[j]):
            possible_rules[j].difference_update(possible_rules[i])
    for j in range(len(possible_rules)-1, -1, -1):
        if len(possible_rules[i]) < len(possible_rules[j]):
            possible_rules[j].difference_update(possible_rules[i])

print(possible_rules)

mul = 1
for i in range(0, 6):
    mul *= my_ticket[possible_rules[i].pop()]
print(mul)
