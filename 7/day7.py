def inside(rule, rules_dict):
    contained = rules_dict.get(rule).split(',')
    if contained[0] == ' no other bags':
        return 0
    bags = []
    for elem in contained:
        bag = elem.strip().split(' ', 1)
        if bag[0] == '1':
            bag[1] = bag[1] + 's'
        bags.append(bag)
    sum = 0
    for bag in bags:
        sum += int(bag[0]) + int(bag[0]) * inside(bag[1], rules_dict)
    return sum

input7 = open('7/input7.txt', 'r+')
lines = input7.readlines()
input7.close()
rules = []
for line in lines:
    rules.append(line.replace('.\n', '').split('contain'))
#Part1
contains = ['shiny gold bags']
unique_colors = set()
rules_dict = dict()
while len(contains) != 0:
    current = contains.pop()
    for rule in rules:
        rules_dict.update({rule[0].strip() : rule[1]})
        contained = rule[1].split(',')
        for elem in contained:
            bag = elem.strip().split(' ', 1)
            if bag[0] == '1':
                bag[1] = bag[1] + 's'
            if bag[1] == current:
                contains.append(rule[0].strip())
                unique_colors.add(rule[0].strip())
print(len(unique_colors))
#Part2
amount = inside('shiny gold bags', rules_dict)
print(amount)