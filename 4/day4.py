import re

class Passport:
    def __init__(self, entry):
        self.byr = '0'
        self.iyr = '0'
        self.eyr = '0'
        self.hgt = '0'
        self.hcl = '0'
        self.ecl = '0'
        self.pid = '0'
        self.cid = '0'
        fields = entry.split(' ')
        for field in fields:
            temp = field.split(':')
            opt = temp[0]
            val = temp[1]
            if opt == 'byr':
                self.byr = val
            elif opt == 'iyr':
                self.iyr = val
            elif opt == 'eyr':
                self.eyr = val
            elif opt == 'hgt':
                self.hgt = val
            elif opt == 'hcl':
                self.hcl = val
            elif opt == 'ecl':
                self.ecl = val
            elif opt == 'pid':
                self.pid = val
            elif opt == 'cid':
                self.cid = val

    def isOptionalValid(self):
        if self.byr == '0':
            return False
        elif self.iyr == '0':
            return False
        elif self.eyr == '0':
            return False
        elif self.hgt == '0':
            return False
        elif self.hcl == '0':
            return False
        elif self.ecl == '0':
            return False
        elif self.pid == '0':
            return False
        else:
            return True

    def isValid(self):
        if self.isOptionalValid() == False:
            return False
        else:
            if int(self.byr) < 1920 or int(self.byr) > 2002:
                return False
            elif int(self.iyr) < 2010 or int(self.iyr) > 2020:
                return False
            elif int(self.eyr) < 2020 or int(self.eyr) > 2030:
                return False
            elif self.isValidHeight() == False:
                return False
            elif self.isValidHairColor() == False:
                return False
            elif self.ecl != 'amb' and self.ecl != 'blu' and self.ecl != 'brn' and self.ecl != 'gry' and self.ecl != 'grn' and self.ecl != 'hzl' and self.ecl != 'oth':
                return False
            elif len(self.pid) != 9:
                return False
        return True

    def isValidHairColor(self):
        pattern = re.compile('^#([a-fA-F0-9]{6})$')
        if re.search(pattern, self.hcl):
            return True
        else:
            return False

    def isValidHeight(self):
        temp1 = self.hgt.split('c')
        if len(temp1) == 1:
            temp2 = self.hgt.split('i')
            if len(temp2) == 1:
                return False
            else:
                height = int(temp2[0])
                if height < 59 or height > 76:
                    return False
        else:
            height = int(temp1[0])
            if height < 150 or height > 193:
                return False
        return True

input4 = open("4/input4.txt", "r+")
lines = input4.readlines()
input4.close()
entries = []
temp_string = ''
for line in lines:
    if line == '\n':
        entries.append(temp_string.strip())
        temp_string = ''
    else:
        temp_string = temp_string + line.replace('\n', ' ')
counter1 = 0
counter2 = 0
for entry in entries:
    pp = Passport(entry)
    if pp.isOptionalValid():
        counter1 += 1
    if pp.isValid():
        counter2 += 1
print(counter1)
print(counter2)