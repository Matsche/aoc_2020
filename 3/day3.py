input2 = open("3/input3.txt", "r+")
lines = input2.readlines()
input2.close()
index = 0
counter = 0
for i in range(2, len(lines), 2):
    index = index + 1
    if index > 30:
        index = index - 31
    if lines[i][index] == '#':
        counter = counter + 1
print(counter)    
# (1,1) = 90 (3,1) = 244 (5,1) = 97 (7,1) = 92 (1,2) = 48