import math

def lcm(x, y):
    return (x*y) // math.gcd(x,y)

inputD = open('D/inputD.txt', 'r+')
lines = inputD.readlines()
inputD.close()

sched = lines[1].strip('\n').split(',')
timings = []
offset = []
for i in range(0, len(sched)):
    if sched[i] != 'x':
        timings.append(int(sched[i]))
        offset.append(i)

inc = timings[0]
pos = 0
for j in range(0, len(timings)):
    while (pos + offset[j]) % timings[j]:
        pos += inc
    inc = lcm(inc, timings[j])
print(pos)