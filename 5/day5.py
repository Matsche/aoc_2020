input5 = open('5/input5.txt', '+r')
lines = input5.readlines()
input5.close
uids = []
for line in lines:
    row_info = line[:7]
    col_info = line[7:]
    row = 0
    col = 0
    upper = 128
    lower = 0
    for i in range(0,len(row_info)-1):
        if row_info[i] == 'F':
            upper = (upper + lower) // 2
        else:
            lower = (lower + upper) // 2
    if row_info[6] == 'F':
        row = lower
    else:
        row = upper-1
    upper = 8
    lower = 0
    for i in range(0,len(col_info)-1):
        if col_info[i] == 'L':
            upper = (upper + lower) // 2
        else:
            lower = (lower + upper) // 2
    if row_info[6] == 'L':
        col = lower
    else:
        col = upper-1
    uids.append(row * 8 + col)
uids.sort()
print(uids[-1])
i = 48
missing = []
for id in uids:
    if id != i:
        missing.append(i)
        i += 1
    i += 1
print(missing)