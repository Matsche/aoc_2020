input2 = open("2/input2.txt", "r+")
lines = input2.readlines()
input2.close()
counter1 = 0
counter2 = 0
for line in lines:
    temp = line.split(":")
    restriction= temp[0].split(" ")
    password = temp[1]
    num_range = restriction[0].split("-")
    occ = password.count(restriction[1])
    if occ >= int(num_range[0]) and occ <= int(num_range[1]):
        counter1 = counter1 + 1
    if (password[int(num_range[0])] == restriction[1] and password[int(num_range[1])] != restriction[1]) or (password[int(num_range[0])] != restriction[1] and password[int(num_range[1])] == restriction[1]):
        counter2 = counter2 + 1
print("Count1: " + str(counter1) + "\n")
print("Count2: " + str(counter2) + "\n")